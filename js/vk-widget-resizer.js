function vkWidgetInit() {
    var containerWidth = document.querySelector('#vk_widget').clientWidth;

    document.querySelector('#vk_widget').innerHTML = '<div id="vk_groups"></div>';

    VK.Widgets.Group("vk_groups", {
        mode: 4,
        no_cover: 1,
        color3: '507299',
        width: containerWidth,
        height: '1200',
    }, 167128717);
}

/* begin Document Ready */
document.addEventListener('DOMContentLoaded', function() {

    vkWidgetInit();

    /* begin Window Resize */
    (function() {
        window.addEventListener("resize", resizeThrottler, false);

        var resizeTimeout;

        function resizeThrottler() {
            if (!resizeTimeout) {
                resizeTimeout = setTimeout(function() {
                    resizeTimeout = null;
                    actualResizeHandler();
                }, 66);
            }
        }

        function actualResizeHandler() {
            vkWidgetInit();
        }

    })();
    /* end Window Resize */
});
/* end Document Ready */